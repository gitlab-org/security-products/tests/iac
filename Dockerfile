FROM ubuntu
RUN export nodejs_version="8.2" \
&& apt-get update && apt-get -y install nodejs="$node_version"
COPY package.json usr/share/src/app
RUN cd /usr/src/app \
&& npm install node-static

EXPOSE 80000
CMD ["npm", "start"]

WORKDIR .

RUN sudo echo "but actually, don't use sudo"

ENTRYPOINT bash
ENTRYPOINT sh

USER root
